from django.http import HttpResponse, HttpResponseRedirect
from user.models import Profile, Reviewable
from projects.models import TaskOffer
from .models import (
    Project,
    Task,
    TaskFile,
    TaskOffer,
    Delivery,
    ProjectCategory,
    Team,
    TaskFileTeam,
    directory_path,
)
from .forms import (
    ProjectForm,
    TaskFileForm,
    ProjectStatusForm,
    TaskOfferForm,
    TaskOfferResponseForm,
    TaskPermissionForm,
    DeliveryForm,
    TaskDeliveryResponseForm,
    TeamForm,
    TeamAddForm,
)
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core import mail
from django.db import transaction
from django.contrib import messages

def projects(request):
    projects = Project.objects.all()
    project_categories = ProjectCategory.objects.all()
    return render(
        request,
        "projects/projects.html",
        {"projects": projects, "project_categories": project_categories,},
    )

def send_emails(request, project):
    from django.contrib.sites.shortcuts import get_current_site
    current_site = get_current_site(request)
    category_id = project.category
    people = Profile.objects.filter(categories__id=project.category.id)
    for person in people:
        if person.user.email:
            try:
                with mail.get_connection() as connection:
                    mail.EmailMessage(
                        "New Project: " + project.title,
                        "A new project you might be interested in was created and can be viwed at "
                        + current_site.domain
                        + "/projects/"
                        + str(project.id),
                        "Agreelancer",
                        [person.user.email],
                        connection=connection,
                    ).send()
            except Exception as e:
                messages.success(
                    request,
                    "Sending of email to "
                    + person.user.email
                    + " failed: "
                    + str(e),
                )

@login_required
def new_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(commit=False)
            project.user = request.user.profile
            project.category = get_object_or_404(
                ProjectCategory, id=request.POST.get("category_id")
            )
            project.save()

            send_emails(request, project)
            task_title = request.POST.getlist("task_title")
            task_description = request.POST.getlist("task_description")
            task_budget = request.POST.getlist("task_budget")
            success = Task.create_fromlist(task_title, task_description,
                task_budget, project)

            #Dont create project if any task has negative
            if success:
               return redirect("project_view", project_id=project.id)
            else:
                project.delete()

    form = ProjectForm()
    return render(request, "projects/new_project.html", {"form": form})

def deny_other_offers(taskoffer):
    offers = TaskOffer.objects.filter(task = taskoffer.task)
    offers= offers.exclude(id = taskoffer.id)
    for offer in offers:
        offer.status = "d"
        offer.save()

def create_offer_response(request, project):
    taskoffer_id = request.POST.get("taskofferid")
    instance = get_object_or_404(TaskOffer, id=taskoffer_id)
    offer_response_form = TaskOfferResponseForm(request.POST, instance=instance)
    old_offer = TaskOffer.objects.get(id = taskoffer_id)
    if offer_response_form.is_valid() and old_offer.status != 'd':
        offer_response = offer_response_form.save(commit=False)

        if offer_response.status == "a":
            offer_response.task.read.add(offer_response.offerer)
            offer_response.task.write.add(offer_response.offerer)
            project = offer_response.task.project
            project.participants.add(offer_response.offerer)
            deny_other_offers(offer_response)


        offer_response.save()

def change_status(request, project):
    status_form = ProjectStatusForm(request.POST)
    if status_form.is_valid():
        project_status = status_form.save(commit=False)
        project.status = project_status.status
        project.save()
        if project.status == "f":
            for taskOffer in TaskOffer.objects.filter(task__project = project, status="a"):
                Reviewable.create_reviewables(project, taskOffer)
        return project_status

def submit_offer(request):
    task_offer_form = TaskOfferForm(request.POST)
    if task_offer_form.is_valid():
        task_offer = task_offer_form.save(commit=False)
        task_offer.task = Task.objects.get(pk=request.POST.get("taskvalue"))
        task_offer.offerer = request.user.profile
        if task_offer.price > task_offer.task.budget:
            messages.warning(request,"task offer price can not exceed task budget")
        else:
            task_offer.save()

@login_required
def project_view(request, project_id):
    template = "projects/project_view.html"
    project = Project.objects.get(pk=project_id)
    tasks = project.tasks.all()
    total_budget = 0  # Initializes the total budget to 0
    for item in tasks:
        total_budget += item.budget
    context = {
        "project" : project,
        "tasks": tasks,
        "total_budget": total_budget
    }
    if isProjectOwner(request.user, project):
        if request.method == "POST" and "offer_response" in request.POST:
            create_offer_response(request, project)
        elif request.method == "POST" and "status_change" in request.POST:
            change_status(request, project)
        context["status_form"] = ProjectStatusForm(initial={"status": project.status})
        context["offer_response_form"] = TaskOfferResponseForm()
    else:
        context["task_offer_form"] = TaskOfferForm()
        if request.method == "POST" and "offer_submit" in request.POST:
            submit_offer(request)
    return render(request, template, context)

def isProjectOwner(user, project):
    return user == project.user.user

def can_modify_file(user, task, task_file):
    user_permissions = get_user_task_permissions(user, task)
    access = user_permissions["modify"] or user_permissions["owner"]
    existing_file = task.files.filter(
        file=directory_path(task_file, task_file.file.file)
    ).first()
    for team in user.profile.teams.all():
        file_modify_access = TaskFileTeam.objects.filter(
            team=team, file=existing_file, modify=True
        ).exists()
        access = access or file_modify_access
    return access

def save_file(task_file, project, task, upload_profile):
    existing_file = task.files.filter(
        file=directory_path(task_file, task_file.file.file)
    ).first()
    if existing_file:
        existing_file.delete()
    task_file.save()
    accepted_task_offer = task.accepted_task_offer()
    if (
        upload_profile != project.user
        and upload_profile != accepted_task_offer.offerer
    ):
        teams = upload_profile.teams.filter(task__id=task.id)
        for team in teams:
            tft = TaskFileTeam()
            tft.team = team
            tft.file = task_file
            tft.read = True
            tft.save()

@login_required
def upload_file_to_task(request, project_id, task_id):
    project = Project.objects.get(pk=project_id)
    task = Task.objects.get(pk=task_id)
    user_permissions = get_user_task_permissions(request.user, task)
    if not (
        user_permissions["modify"]
        or user_permissions["write"]
        or user_permissions["upload"]
        or isProjectOwner(request.user, project)
    ):
        return redirect("/user/login")
    if request.method == "POST":
        task_file_form = TaskFileForm(request.POST, request.FILES)
        if task_file_form.is_valid():
            task_file = task_file_form.save(commit=False)
            task_file.task = task
            if can_modify_file(request.user, task, task_file):
                save_file(task_file, project, task, request.user.profile)
            else:
                messages.warning(
                    request, "You do not have access to modify this file"
                )
        return redirect("task_view", project_id=project_id, task_id=task_id)

    task_file_form = TaskFileForm()
    return render(
        request,
        "projects/upload_file_to_task.html",
        {"project": project, "task": task, "task_file_form": task_file_form,},
    )

def get_user_task_permissions(user, task):
    user_permissions = {
        "write": True,
        "read": True,
        "modify": True,
        "owner": True,
        "upload": True,
    }

    if user == task.project.user.user:
        return user_permissions
    user_permissions["owner"] = False

    if (
        task.accepted_task_offer()
        and task.accepted_task_offer().offerer == user.profile
    ):
        return user_permissions

    user_permissions["read"] = user.profile.task_participants_read.filter(id=task.id).exists()
    user_permissions["upload"] = user.profile.teams.filter(task__id=task.id, write=True).exists()
    user_permissions["view_task"] = user.profile.teams.filter(task__id=task.id).exists()
    user_permissions["write"] =  user.profile.task_participants_write.filter(id=task.id).exists()
    user_permissions["modify"] = user.profile.task_participants_modify.filter(id=task.id).exists()

    return user_permissions

def isAcceptedOfferer(user, task):
    accepted_task_offer = task.accepted_task_offer()
    return accepted_task_offer and accepted_task_offer.offerer == user.profile

def task_delivery(request, task):
    deliver_form = DeliveryForm(request.POST, request.FILES)
    if deliver_form.is_valid():
        delivery = deliver_form.save(commit=False)
        delivery.task = task
        delivery.delivery_user = request.user.profile
        delivery.save()
        task.status = "pa"
        task.save()

def task_delivery_response(request, task):
    instance = get_object_or_404(Delivery, id=request.POST.get("delivery-id"))
    deliver_response_form = TaskDeliveryResponseForm(
        request.POST, instance=instance
    )
    if deliver_response_form.is_valid():
        delivery = deliver_response_form.save()
        from django.utils import timezone

        delivery.responding_time = timezone.now()
        delivery.responding_user = request.user.profile
        delivery.save()

        if delivery.status == "a":
            task.status = "pp"
            task.save()
        elif delivery.status == "d":
            task.status = "dd"
            task.save()

def task_team(request,task):
    team_form = TeamForm(request.POST)
    if team_form.is_valid():
        team = team_form.save(False)
        team.task = task
        team.save()

def task_team_add(request,task):
    instance = get_object_or_404(Team, id=request.POST.get("team-id"))
    team_add_form = TeamAddForm(request.POST, instance=instance)
    if team_add_form.is_valid():
        team = team_add_form.save(False)
        team.members.add(*team_add_form.cleaned_data["members"])

def task_change_permissions(request,task):
    for t in task.teams.all():
        for f in task.files.all():
            try:
                tft_string = "permission-perobj-" + str(f.id) + "-" + str(t.id)
                tft_id = request.POST.get(tft_string)
                instance = TaskFileTeam.objects.get(id=tft_id)
            except Exception as e:
                instance = TaskFileTeam(file=f, team=t,)

            permission_string =  "permission-{}-" + str(f.id) + "-" + str(t.id)
            instance.read = request.POST.get(permission_string.format("read"))
            instance.write = request.POST.get(permission_string.format("write"))
            instance.modify = request.POST.get(permission_string.format("modify"))
            instance.save()
        t.write = request.POST.get("permission-upload-" + str(t.id)) or False
        t.save()

@login_required
def task_view(request, project_id, task_id):
    template = "projects/task_view.html"
    login = "/user/login"
    user = request.user
    project = Project.objects.get(pk=project_id)
    task = Task.objects.get(pk=task_id)
    accepted_task_offer = task.accepted_task_offer()

    user_permissions = get_user_task_permissions(request.user, task)
    if (
        not user_permissions["read"]
        and not user_permissions["write"]
        and not user_permissions["modify"]
        and not user_permissions["owner"]
        and not user_permissions["view_task"]
    ):
        return redirect(login)
    if isAcceptedOfferer(user, task):
        if request.method == "POST" and "delivery" in request.POST:
            task_delivery(request, task)
        if request.method == "POST" and "team" in request.POST:
            task_team(request, task)
        if request.method == "POST" and "team-add" in request.POST:
            task_team_add(request, task)
        if request.method == "POST" and "permissions" in request.POST:
            task_change_permissions(request,task)
    elif isProjectOwner(user, project):
        if request.method == "POST" and "delivery-response" in request.POST:
            task_delivery_response(request, task)

    deliveries = task.delivery.all()
    team_files = []
    teams = user.profile.teams.filter(task__id=task.id).all()
    per = {}
    for f in task.files.all():
        per[f.name()] = {}
        for p in f.teams.all():
            per[f.name()][p.team.name] = p
            if p.read:
                team_files.append(p)
    context = {
                "task": task,
                "project": project,
                "user_permissions": user_permissions,
                "deliver_form": DeliveryForm(),
                "deliveries": deliveries,
                "deliver_response_form": TaskDeliveryResponseForm(),
                "team_form": TeamForm(),
                "team_add_form": TeamAddForm(),
                "team_files": team_files,
                "per": per,
    }
    return render(request, template, context)

@login_required
def task_permissions(request, project_id, task_id):
    task = Task.objects.get(pk=task_id)
    project = Project.objects.get(pk=project_id)
    if request.user == project.user.user or request.user == task.accepted_task_offer().offerer.user:
        assert int(project_id) == task.project.id
        if request.method == "POST":
            task_permission_form = TaskPermissionForm(request.POST)
            if task_permission_form.is_valid():
                set_task_permissions(task_permission_form, task)
                return redirect("task_view", project_id=project_id, task_id=task_id)

        task_permission_form = TaskPermissionForm()
        return render(
            request,
            "projects/task_permissions.html",
            {"project": project, "task": task, "form": task_permission_form,},
        )
    return redirect("task_view", project_id=project_id, task_id=task_id)

def set_task_permissions(task_permission_form, task):
    try:
        username = task_permission_form.cleaned_data["user"]
        user = User.objects.get(username=username)
        permission_type = task_permission_form.cleaned_data[
            "permission"
        ]
        if permission_type == "Read":
            task.read.add(user.profile)
        elif permission_type == "Write":
            task.write.add(user.profile)
        elif permission_type == "Modify":
            task.modify.add(user.profile)
    except Exception:
        print("user not found")

@login_required
def delete_file(request, file_id):
    f = TaskFile.objects.get(pk=file_id)
    f.delete()
    return HttpResponseRedirect(request.META.get("HTTP_REFERER"))

