from django.test import TestCase, RequestFactory, Client
from projects.views import project_view
from user.views import profile
from projects.models import (
    Project,
    Task,
    TaskOffer,
    ProjectCategory
)
from django.contrib.auth.models import User
from user.models import Profile, Review, Reviewable
from django.contrib.messages.storage.fallback import FallbackStorage

class ReviewTestCase(TestCase):

    """ 
    Integration and system test of Review feature. Path-based coverage 
    or multiple unit tests is used for this test.
    
    related functional requirements:

    A user should be able to score another user that
    they have worked on a project with from 1-5.

    A user should be able to see the average score of every
    other user by going to user's page.

    The system should store the score of users.

    A user should be notified when they get a review.

    The system should add a new review to the total score
    of the users by adding the scores from all the reviews
    of that user and taking the average value.
    """

    def setUp(self):
        """
        set up requestfactory
        set up category
        Set up project,  projectowner user,  two offerer users.
        set up two tasks for the project.
        """

        self.factory = RequestFactory()
        category = ProjectCategory(name="testCategory")
        category.save()
        projectuser = User.objects.create_user(
            username='testProjectOwner',
            password='qwerty',
        )
        projectuser.save()
        projectOwner = Profile(
            id=1,
            user=projectuser,
        )
        projectOwner.categories.add(category)
        projectOwner.save()

        offereruser1 = User.objects.create_user(
            username='testProjectOfferer1',
            password='qwerty',
        )
        offereruser1.save()
        projectOfferer1 = Profile(
            id=2,
            user=offereruser1,
        )
        projectOfferer1.categories.add(category)
        projectOfferer1.save()

        offereruser2 = User.objects.create_user(
            username='testProjectOfferer2',
            password='qwerty',
        )
        offereruser2.save()
        projectOfferer2 = Profile(
            id=3,
            user=offereruser2,
        )
        projectOfferer2.categories.add(category)
        projectOfferer2.save()


        project = Project(
            user=projectOwner,
            title="testProject",
            description="test project description",
            category=category,
        )
        project.save()

        task1 = Task(
            project=project,
            title="test task 1",
            description="description for test task",
            budget=200
        )
        task1.save()

        task2 = Task(
            project=project,
            title="test task 2",
            description="description for test task 2",
            budget=200
        )
        task2.save()
        

        self.data = {
            "offer_response" : "",
            "feedback" : "contains feedback",
            "taskofferid" : "1",
            "status" : "a"
        }

    def get_request(self, data):
        request = self.factory.post("dummy_url", data.copy())
        setattr(request, 'session', 'session')
        messages = FallbackStorage(request)
        setattr(request, '_messages', messages)
        return request


    def test_review(self):
        projectowner = Profile.objects.get(id=1).user
        offerer1 = Profile.objects.get(id=2).user
        offerer2 = Profile.objects.get(id=3).user

        # Submit new task offers from two different offerers on  seperate tasks
        offer_submit1 =  {
            "title" : "title",
            "description" : "description",
            "price" : "20",
            "taskvalue" : "1",
            "offer_submit" : ""
        }

        offer_submit2 =  {
            "title" : "title",
            "description" : "description",
            "price" : "20",
            "taskvalue" : "2",
            "offer_submit" : ""
        }

        request1 = self.get_request(offer_submit1)
        request2 = self.get_request(offer_submit2)
        request1.user = offerer1
        request2.user = offerer2

        project_view(request1, 1)
        project_view(request2, 1)

        # accept new offers

        offer_response1 = {
            "offer_response" : "",
            "feedback" : "contains feedback",
            "taskofferid" : "1",
            "status" : "a"
        }

        offer_response2 = {
            "offer_response" : "",
            "feedback" : "contains feedback",
            "taskofferid" : "2",
            "status" : "a"
        }

        request3 = self.get_request(offer_response1)
        request4 = self.get_request(offer_response2)
        request3.user = projectowner
        request4.user = projectowner

        project_view(request3, 1)
        project_view(request4, 1)

        # fihish the project

        status_change = {
            "status_change" : "",
            "status" : "f"
        }
        
        request5 = self.get_request(status_change)
        request5.user = projectowner
        project_view(request5, 1)

        #Test for created reviewables, there should be four 
        count = Reviewable.objects.all().count()
        self.assertEqual(count, 4)

        # Perform reviews:
        # user1 and user2 reviews projectowner rating 2 and 4
        # projectowner reviers offerer2 with rating 4

        review2 = { 
                "review" : "",
                "score" : "2"
            }
        
        review4 = { 
                "review" : "",
                "score" : "4"
            }
        
        revreq1 = self.get_request(review2)
        revreq2 = self.get_request(review4)
        revreq1.user = offerer1
        revreq2.user = offerer2

        revreq3 = self.get_request(review4)
        revreq3.user = projectowner

        revreq4 = self.get_request(review2)
        revreq4.user = projectowner

        profile(revreq1, "testProjectOwner")
        profile(revreq2, "testProjectOwner")
        profile(revreq3, "testProjectOfferer2")
        profile(revreq4, "testProjectOfferer1")

        # ensure that admin has two reviews made and stored, and offerer 1 and offerer 2 has one review each.

        owner_reviews = Review.objects.filter(user = projectowner).count()
       
        offerer2_reviews = Review.objects.filter(user = offerer2).count()
        self.assertEqual(owner_reviews, 2)
        self.assertEqual(offerer2_reviews, 1 )

        #use django client to ensure that review average score is properly calculated and passed to template

        c = Client()
        ownerpage =  c.get("/user/users/testProjectOwner/")
        offerer1page = c.get("/user/users/testProjectOfferer1/")
        offerer2page = c.get("/user/users/testProjectOfferer2/")
    
        self.assertEqual(3, ownerpage.context["score"])
        self.assertEqual(2, offerer1page.context["score"])
        self.assertEqual(4, offerer2page.context["score"]) 

    
        

        


        
                


    



