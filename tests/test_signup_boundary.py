from django.test import TestCase, RequestFactory
from projects.models import ProjectCategory
from user.models import Profile
from user.views import signup
from django.contrib.auth.models import User
from django.contrib.messages.storage.fallback import FallbackStorage

class SignUpBoundaryTestCase(TestCase):
    '''
    tests boundary values for username and password in signup
    '''


    def setUp(self):
        category = ProjectCategory(name="testCategory")
        category.save()
        self.factory = RequestFactory()
        #valid data overridden in each test function
        #to test each field
        self.data = {
            "username": "user",
            "first_name": "firstname",
            "last_name": "lastname",
            "categories": [1],
            "company": "company",
            "email": "test@mail.net",
            "email_confirmation": "test@mail.net",
            "password1": "SuperPassTest",
            "password2": "SuperPassTest",
            "phone_number": "1234",
            "country": "testland",
            "state": "teststate",
            "city":"testcity",
            "postal_code":"1323",
            "street_address":"teststreet",
            'captcha_0': 'dummy-value',
            'captcha_1': 'PASSED',
        }



    def test_valid_usernames(self):
        username_lengths = [149, 150]
        usernames = []
        for length in username_lengths:
            usernames.append("i" * length)
        for username in usernames:
            self.data["username"] = username
            request = self.get_request(self.data)
            signup(request)
            profile =  User.objects.filter(username=self.data['username'])
            assert profile.exists()
            profile.delete()



    def test_invalid_usernames(self):
        username_lengths = [0, 151]
        usernames = []
        for length in username_lengths:
            usernames.append("i" * length)
        for username in usernames:
            self.data["username"] = username
            request = self.get_request(self.data)
            signup(request)
            profile =  User.objects.filter(username=self.data['username'])
            assert not profile.exists()


    def test_valid_password(self):
        #8 length and 9 length boundary test
        valid_values = ["avjfkglp","aldgpmnpp"]
        for value in valid_values:
            self.data["password1"] = value
            self.data["password2"] = value
            request = self.get_request(self.data)
            signup(request)
            profile = User.objects.filter(username=self.data['username'])
            assert profile.exists()
            profile.delete()



    def test_invalid_passwords(self):
        #7 length boundary test
        invalid_values = ["mvskklg", ""]
        for value in invalid_values:
            self.data["password1"] = value
            self.data["password2"] = value
            request =self.get_request(self.data)
            signup(request)
            assert not User.objects.filter(username=self.data['username']).exists()

    def get_request(self, data):
        request = self.factory.post("dummy_url", data)
        setattr(request, 'session', 'session')
        messages = FallbackStorage(request)
        setattr(request, '_messages', messages)
        return request