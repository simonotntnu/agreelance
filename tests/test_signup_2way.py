from django.test import TestCase, RequestFactory
from projects.models import ProjectCategory
from user.models import Profile
from user.views import signup
from django.contrib.auth.models import User
from django.contrib.messages.storage.fallback import FallbackStorage

class SignUpTwoWayTestCase(TestCase):
    '''
    tests boundary values for username and password in signup
    '''


    def setUp(self):
        category = ProjectCategory(name="testCategory")
        category.save()
        self.factory = RequestFactory()
        #valid data overridden in each test function
        #to test each field
        self.data = {
            "username": "user",
            "first_name": "firstname",
            "last_name": "lastname",
            "categories": [1],
            "company": "company",
            "email": "test@mail.net",
            "email_confirmation": "test@mail.net",
            "password1": "SuperPassTest",
            "password2": "SuperPassTest",
            "phone_number": "1234",
            "country": "testland",
            "state": "teststate",
            "city":"testcity",
            "postal_code":"1323",
            "street_address":"teststreet",
            'captcha_0': 'dummy-value',
            'captcha_1': 'PASSED',
        }
        #tuple (value, boolean shouldWork) if shouldwork is true then the test should
        #work if that value is used (or rather not work if shouldwork is false)
        username_domains = [
            ("i"*149, True),
            ("i"*150, True),
            ("i"*151, False),
        ]
        password_domains = [
            ("akdgrdf", False), #length 7 not acceptable
            ("ackfdsjg", True), #length 8 acceptable
            ("aasdkkgsd", True),#length 9 acceptable
        ]
        mail_domains = [
            ("test@mail.net", True),
            ("mailtest", False),
        ]
        #values to be tested at the same time to cover 2-way domain testing
        #covers all possible pairs of 2
        self.test_pairs = [
            [username_domains[0], password_domains[0], mail_domains[0]],
            [username_domains[0], password_domains[1], mail_domains[1]],
            [username_domains[0], password_domains[2], mail_domains[0]],
            [username_domains[1], password_domains[0], mail_domains[1]],
            [username_domains[1], password_domains[1], mail_domains[0]],
            [username_domains[1], password_domains[2], mail_domains[1]],
            [username_domains[2], password_domains[0], mail_domains[0]],
            [username_domains[2], password_domains[1], mail_domains[1]],
            [username_domains[2], password_domains[2], mail_domains[0]],
        ]
    def test_2way_pairs(self):
        for pair in self.test_pairs:
            #should fail if one of the entries should fail
            expected_result = pair[0][1] and pair[1][1] and pair[2][1]
            self.data["username"] = pair[0][0]
            self.data["password1"] = pair[1][0]
            self.data["password2"] = pair[1][0]
            self.data["email"] = pair[2][0]
            self.data["email_confirmation"] = pair[2][0]
            request = self.get_request(self.data)
            signup(request)
            profile =  User.objects.filter(username=self.data['username'])
            assert profile.exists() == expected_result
            if profile:
                profile.delete()

    def get_request(self, data):
        request = self.factory.post("dummy_url", data)
        setattr(request, 'session', 'session')
        messages = FallbackStorage(request)
        setattr(request, '_messages', messages)
        return request