from django.test import TestCase
from projects.views import get_user_task_permissions
from projects.models import Task, Project, ProjectCategory, TaskOffer
from user.models import Profile
from django.contrib.auth.models import User

class GetUserTaskPermissionsTestCase(TestCase):
    '''
    gives full statement coverage for get_user_task_permissions
    '''


    def setUp(self):
        category = ProjectCategory(name="testCategory")
        category.save()
        projectuser = User.objects.create_user(
            username='testProjectOwner',
            password='qwerty',
        )
        projectuser.save()
        projectOwner = Profile(
            id=1,
            user=projectuser,
        )
        projectOwner.categories.add(category)
        projectOwner.save()

        offereruser = User.objects.create_user(
            username='testProjectOfferer',
            password='qwerty',
        )
        offereruser.save()
        projectOfferer = Profile(
            id=2,
            user=offereruser,
        )
        projectOfferer.categories.add(category)
        projectOfferer.save()


        project = Project(
            user=projectOwner,
            title="testProject",
            description="test project description",
            category=category,
        )
        project.save()

        task = Task(
            project=project,
            title="test task",
            description="description for test task",
            budget=200
        )
        task.save()

        taskOffer = TaskOffer(
            task=task,
            title="Test Task Offer",
            description="description of offer",
            price=50,
            offerer=projectOfferer,
            status="a",
            feedback="test feedback"
        )
        taskOffer.save()

        randomuser = User.objects.create_user(
            username='testRandomUser',
            password='qwerty',
        )
        randomuser.save()
        randomProfile = Profile(
            id=3,
            user=randomuser,
        )
        randomProfile.categories.add(category)
        randomProfile.save()


    def tearDown(self):
        pass

    def test_user_is_owner(self):
        '''
        This test covers until the first return
        '''
        task = Task.objects.get(title="test task")
        user = Profile.objects.get(id=1).user
        expected_permissions = {
            "write": True,
            "read": True,
            "modify": True,
            "owner": True,
            "upload": True,
        }
        permissions = get_user_task_permissions(user, task)
        self.assertEqual(expected_permissions, permissions)



    def test_user_is_offerer(self):
        '''
        This test covers from after the first return to the second
        '''
        task = Task.objects.get(title="test task")
        user = Profile.objects.get(id=2).user
        expected_permissions = {
            "write": True,
            "read": True,
            "modify": True,
            "owner": False,
            "upload": True,
        }
        permissions = get_user_task_permissions(user, task)
        self.assertEqual(expected_permissions, permissions)

    def test_user_is_neither(self):
        '''
        If the user is not related to the project in any way then there should be no permissions
        This test covers everything beneath the second return
        '''
        task = Task.objects.get(title="test task")
        user = Profile.objects.get(id=3).user
        expected_permissions = {
            "write": False,
            "read": False,
            "modify": False,
            "owner": False,
            "view_task": False,
            "upload": False,
        }
        permissions = get_user_task_permissions(user, task)
        self.assertEqual(expected_permissions, permissions)


