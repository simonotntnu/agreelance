from django.test import TestCase, RequestFactory
from projects.views import project_view
from projects.models import (
    Project,
    Task,
    TaskOffer,
    ProjectCategory
)
from django.contrib.auth.models import User
from user.models import Profile
from django.contrib.messages.storage.fallback import FallbackStorage

class ProjectAcceptOfferTestCase(TestCase):

    """ 
    
    related functional requirements:

    FR5
    Accept bids/offer
    The customer should accept the offer for each task from one freelancer-project manager.

   FR8
   Follow the project status
   The customer should be able to follow the status of the project and change
   the status of the project into “In Progress” when the offers for the tasks of the project have accepted

   Desired output coverage:
        Accept one offer for one task only.
        can only accept offer when project is open, not when its in progress or finished.



    """

    def setUp(self):
        """
        set up requestfactory
        set up category
        Set up project,  projectowner user,  offerer user.
        set up one tasks for the project.
        set up taskoffer.
        """

        self.factory = RequestFactory()
        category = ProjectCategory(name="testCategory")
        category.save()
        projectuser = User.objects.create_user(
            username='testProjectOwner',
            password='qwerty',
        )
        projectuser.save()
        projectOwner = Profile(
            id=1,
            user=projectuser,
        )
        projectOwner.categories.add(category)
        projectOwner.save()

        offereruser1 = User.objects.create_user(
            username='testProjectOfferer1',
            password='qwerty',
        )
        offereruser1.save()
        projectOfferer1 = Profile(
            id=2,
            user=offereruser1,
        )
        projectOfferer1.categories.add(category)
        projectOfferer1.save()

        offereruser2 = User.objects.create_user(
            username='testProjectOfferer2',
            password='qwerty',
        )
        offereruser2.save()
        projectOfferer2 = Profile(
            id=3,
            user=offereruser2,
        )
        projectOfferer2.categories.add(category)
        projectOfferer2.save()


        project = Project(
            user=projectOwner,
            title="testProject",
            description="test project description",
            category=category,
        )
        project.save()

        task = Task(
            project=project,
            title="test task",
            description="description for test task",
            budget=200
        )
        task.save()

        taskOffer1 = TaskOffer(
            task=task,
            title="Test Task Offer One",
            description="description of offer",
            price=50,
            offerer=projectOfferer1,
            status="o",
            feedback="",
        )
        taskOffer1.save()

        taskOffer2 = TaskOffer(
            task=task,
            title="Test Task Offer One",
            description="description of offer",
            price=50,
            offerer=projectOfferer2,
            status="o",
            feedback="",
        )
        taskOffer2.save()
        

        self.data = {
            "offer_response" : "",
            "feedback" : "contains feedback",
            "taskofferid" : "1",
            "status" : "a"
        }

    def get_request(self, data):
        request = self.factory.post("dummy_url", data.copy())
        setattr(request, 'session', 'session')
        messages = FallbackStorage(request)
        setattr(request, '_messages', messages)
        return request


    def test_accept_two_offers(self):
        """
        It should not be possible to accept two offers or more for one given task.
        input: one task, two offers tried acceped
        output:  only first taskoffer with status a.
        """

        # set up requests
        projectowner = Profile.objects.get(id = 1).user
        
        data2 = self.data.copy()
        data2["taskofferid"] = "2"
        request1 = self.get_request(self.data)
        request2 = self.get_request(data2)
        request1.user = projectowner
        request2.user =  projectowner
        

        # use function to accept offers

        response1 = project_view(request1, 1)
        offerstatus1 = TaskOffer.objects.get(id = 1).status
        offerstatus2 = TaskOffer.objects.get(id = 2).status
        # task should be accepted, other other offers should be denied.
        self.assertEqual('a', offerstatus1)
        self.assertEqual('d', offerstatus2)
    
       
        # try  to accept new offer, should not be acceped and still be set to denied. nothing should change.
        response2 = project_view(request2,1)
        offerstatus2 = TaskOffer.objects.get(id = 2).status
        offerstatus2 = TaskOffer.objects.get(id = 2).status
        self.assertEqual('a', offerstatus1)
        self.assertEqual('d', offerstatus2)


    def accept_offer(self):
        """
        accepting an offer should set taskoffer to accepted,
        and accepted offer should be associated with task.
        """


        # set up requests
        projectowner = Profile.objects.get(id = 1).user
        request = self.get_request(self.data)
        request.user = projectowner
        # use function to accept offers

        response = project_view(request1, 1)
        offerstatus = TaskOffer.objects.get(id = 1).status
        
        self.assertEqual('a', offerstatus)

        # fetch task and associated taskoffer

        taskoffer =  Task.accepted_task_offer(Task.objects.get(id = 1))

        self.assertEqual('a', taskoffer.status)


    def deny_offer(self):
        """
        denying an offer should set taskoffer to denied
        """

        # set up requests
        projectowner = Profile.objects.get(id = 1).user
        data = self.data.copy()
        data["status"] = "d"
        request = self.get_request(data)
   
        request.user = projectowner
        # use function to accept offers

        response = project_view(request1, 1)
        offerstatus = TaskOffer.objects.get(id = 1).status
        self.assertEqual('d', offerstatus)






        



