from django.test import TestCase, RequestFactory
from projects.views import upload_file_to_task
from django.contrib.messages.storage.fallback import FallbackStorage
from projects.models import (
    Project,
    Task,
    TaskOffer,
    Delivery,
    ProjectCategory,
    Team,
    TaskFile,
    TaskFileTeam
)
from django.contrib.auth.models import User, AnonymousUser
from user.models import Profile
from django.core.files import temp as tempfile
from django.core.files.uploadedfile import SimpleUploadedFile

class UploadFileTestCase(TestCase):
    """
    test coverage for uploading a task file

    """

    def setUp(self):
        """
        set up requestfactory
        set up category
        Set up project,  projectowner user,  offerer user.
        set up one tasks for the project.
        set up taskoffer.
        """

        self.factory = RequestFactory()
        category = ProjectCategory(name="testCategory")
        category.save()
        projectuser = User.objects.create_user(
            username='testProjectOwner',
            password='qwerty',
        )
        projectuser.save()
        projectOwner = Profile(
            id=1,
            user=projectuser,
       )
        
        projectOwner.categories.add(category)
        projectOwner.save()

        offereruser = User.objects.create_user(
            username='testProjectOfferer',
            password='qwerty',
        )
        offereruser.save()
        projectOfferer = Profile(
            id=2,
            user=offereruser,
        )
        projectOfferer.categories.add(category)
        projectOfferer.save()


        offereruser2 = User.objects.create_user(
            username='testProjectOfferer2',
            password='qwerty',
        )
        offereruser2.save()
        projectOfferer2 = Profile(
            id=3,
            user=offereruser2,
        )
        projectOfferer2.categories.add(category)
        projectOfferer2.save()
        project = Project(
            user=projectOwner,
            title="testProject",
            description="test project description",
            category=category,
        )
        project.save()

        task = Task(
            project=project,
            title="test task",
            description="description for test task",
            budget=200
        )
        task.save()

        taskOffer = TaskOffer(
            task=task,
            title="Test Task Offer One",
            description="description of offer",
            price=50,
            offerer=projectOfferer,
            status="a",
            feedback="",
        )
        task.read.add(projectOfferer)
        task.read.add(projectOfferer2)
        task.modify.add(projectOfferer2)
        task.write.add(projectOfferer)
        project.participants.add(projectOfferer)
        taskOffer.save()

        team = Team(
            name = "testTeam",
            task = task,
            id = 1,
        )
        team.members.add(2)
        team.members.add(3)
        team.save()

    def test_file_upload(self):
        taskWorker = Profile.objects.get(id = 3)
        data= {}
        request = self.get_request(data)
        request.user = taskWorker.user
        tdir = tempfile.gettempdir()
        file = tempfile.NamedTemporaryFile(suffix=".file", dir=tdir)
        file.write(b'aaaaaaaaaaaaaaaaaaa')
        file.seek(0)

        with open(file.name, 'r') as f:
            request.FILES['file'] = SimpleUploadedFile(file.name, f.read().encode())


        resp = upload_file_to_task(request, 1, 1)
        self.assertEqual(resp.status_code, 302)
        tft = TaskFileTeam.objects.get(id = 1)
        self.assertEqual(tft.team, Team.objects.get(id = 1))
        self.assertEqual(tft.file, TaskFile.objects.get(id = 1))
        self.assertEqual(tft.read, True)

    def get_request(self, data):
            request = self.factory.post("dummy_url", data)
            setattr(request, 'session', 'session')
            messages = FallbackStorage(request)
            setattr(request, '_messages', messages)
            return request