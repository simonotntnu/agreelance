from django.test import TestCase, RequestFactory
from projects.views import project_view
from projects.models import (
    Project,
    Task,
    TaskOffer,
    ProjectCategory
)
from django.contrib.auth.models import User
from user.models import Profile
from django.contrib.messages.storage.fallback import FallbackStorage



class ProjectOfferBoundaryTestCase(TestCase):
    """ 
    Tests boundary values related to giving project task offers
    related functional requirements:

    FR3
    Give bids/offerA 
    freelancer-project manager should be able to give an offer for a task



    """

    def setUp(self):
        """
        set up requestfactory
        set up category
        Set up project,  projectowner user,  offerer user.
        set up one tasks for the project.
        set up taskoffer.
        """

        self.factory = RequestFactory()
        category = ProjectCategory(name="testCategory")
        category.save()
        projectuser = User.objects.create_user(
            username='testProjectOwner',
            password='qwerty',
        )
        projectuser.save()
        projectOwner = Profile(
            id=1,
            user=projectuser,
        )
        projectOwner.categories.add(category)
        projectOwner.save()

        offereruser = User.objects.create_user(
            username='testProjectOfferer',
            password='qwerty',
        )
        offereruser.save()
        projectOfferer = Profile(
            id=2,
            user=offereruser,
        )
        projectOfferer.categories.add(category)
        projectOfferer.save()


        project = Project(
            user=projectOwner,
            title="testProject",
            description="test project description",
            category=category,
        )
        project.save()

        task = Task(
            project=project,
            title="test task",
            description="description for test task",
            budget=200
        )
        task.save()

        

        self.data =  {
            "title" : "title",
            "description" : "description",
            "price" : "0",
            "taskvalue" : "1",
            "offer_submit" : ""
        }


    def test_valid_offers(self):
        """
        A  valid offer should not exceed the budget for the task,
        or be negative.
        """
        valid_prices = [0, 1, 199, 200]
        for price in valid_prices:
            self.data["price"] = str(price)
            request = self.get_request(self.data)
            project_view(request, 1)
            task_offer = TaskOffer.objects.filter(id = 1)
            assert task_offer.exists()
            try:
                TaskOffer.objects.delete(id = 1)
            except: 
                pass




    def test_invalid_offers(self):
        """
        negative offered prices and offers that  exceed the budget for
        a given task should be
        """
        invalid_prices = [-200, -1, 201]
        for price in invalid_prices:
            self.data["price"] = str(price)
            request = self.get_request(self.data)
            project_view(request, 1)
            task_offer = TaskOffer.objects.filter(id=1)
            assert not task_offer.exists()
            try:
                TaskOffer.objects.delete(id = 1)
            except: 
                pass


    def get_request(self, data):
        user = Profile.objects.get(id=2).user
        request = self.factory.post("dummy_url", data)
        request.user = user
        setattr(request, 'session', 'session')
        messages = FallbackStorage(request)
        setattr(request, '_messages', messages)
        return request