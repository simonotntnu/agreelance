from django.test import TestCase, RequestFactory, Client
from projects.views import project_view
from user.views import profile, message, inbox
from user.models import Message

from django.contrib.auth.models import User
from user.models import Profile
from django.contrib.messages.storage.fallback import FallbackStorage
from django.contrib.auth import authenticate

class MessageIntegrationTestCase(TestCase):


    def setUp(self):
        #category = ProjectCategory(name="testCategory")
        #category.save()
        self.factory = RequestFactory()
        user1 = User.objects.create_user(
            username='user1',
            password='qwerty',
        )
        #user1.is_authenticated = True
        user1.save()
        user1Profile = Profile(
            id=1,
            user=user1,
        )
        #user1Profile.categories.add(category)
        user1Profile.save()

        user2 = User.objects.create_user(
            username='user2',
            password='qwerty',
        )
        #user2.is_authenticated = True
        user2.save()
        user2Profile = Profile(
            id=2,
            user=user2,
        )
        #user2Profile.categories.add(category)
        user2Profile.save()

    def get_request(self, data):
        request = self.factory.post("dummy_url", data)
        setattr(request, 'session', 'session')
        messages = FallbackStorage(request)
        setattr(request, '_messages', messages)
        return request

    def testMessages(self):
        user1 = authenticate(username='user1', password='qwerty')
        user2 = authenticate(username='user2', password='qwerty')
        senderclient = Client()
        receiverclient = Client()

        data = {
            "message" : "Hello",
        }
        request = self.get_request(data)
        request.user = user1

        message(request, user2.username)

        data = {}
        client = Client()
        logged_in = client.login(username='user2', password='qwerty')

        inboxpage = client.get("/user/users/user2/inbox")
        expected_convos = {user1}
        self.assertEqual(expected_convos, inboxpage.context['convos'])
        messagepage = client.get("/user/users/user1/message")
        expected_messages = Message.objects.filter(receiver=user1, sender=user2) | \
                            Message.objects.filter(receiver=user2, sender=user1)
        expected_messages = expected_messages.order_by('created')
        self.assertEqual(list(expected_messages), list(messagepage.context['all_messages']))

