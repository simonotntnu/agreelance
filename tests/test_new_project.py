from django.test import TestCase, RequestFactory
from projects.views import new_project
from projects.models import (
    Project,
    Task,
    TaskOffer,
    ProjectCategory
)
from django.contrib.auth.models import User
from user.models import Profile
from django.contrib.messages.storage.fallback import FallbackStorage
from django.utils.datastructures import MultiValueDict

class NewProjectTestCase(TestCase):
    """
    Unit test/ coverage for creating new project and assuring that
    it is properly validated, and stored.
    """

    def setUp(self):
        """
        initialize category, user that creates project
        """
        self.factory = RequestFactory()
        category = ProjectCategory(name="testCategory")
        category.save()
        self.projectuser = User.objects.create_user(
            username='testProjectOwner',
            password='qwerty',
        )
        self.projectuser.save()
        self.projectOwner = Profile(
            id=1,
            user=self.projectuser,
        )
        self.projectOwner.categories.add(category)
        self.projectOwner.save()


    def test_valid_new_project(self):
        """
        attempts to create new project with two tasks
        expected outcome: project created and stored.
        user redirected to project view.
        """

        data = {
            "title" : "Project Name", 
            "description" : "Project Description",
            "category_id" : "1",
            "task_title" : ["task 1", "task 2"],
            "task_description" : ["task desc 1", "task desc 2"],
            "task_budget" : ["200", "50"]
        }

        request = self.get_request(data)
        request.user = self.projectuser
        response = new_project(request)
        project_count = Project.objects.all().count()
        task_count = Task.objects.all().count()
        self.assertEqual(project_count, 1)
        self.assertEqual(task_count, 2)
        self.assertEqual(response.status_code, 302)
       
         


    def test_invalid_new_project(self):
        """
        attempts to create a new project with one valid task
        and one invalid task with negative budget.
        expected outcome: project should not be created, return user to
        project create with error message.
        no tasks should be stored.
        """

        data = {
            "title" : "Project Name", 
            "description" : "Project Description",
            "category_id" : "1",
            "task_title" : ["task 1", "task 2"],
            "task_description" : ["task desc 1", "task desc 2"],
            "task_budget" : ["20", "-200"]
        }
        
        request = self.get_request(data)
        request.user = self.projectuser
        response = new_project(request)
        project_count = Project.objects.all().count()
        task_count = Task.objects.all().count()
        self.assertEqual(project_count, 0)
        self.assertEqual(task_count, 0)
        self.assertEqual(response.status_code, 200)
        

    def get_request(self, data):
            request = self.factory.post("dummy_url", data)
            setattr(request, 'session', 'session')
            messages = FallbackStorage(request)
            setattr(request, '_messages', messages)
            return request