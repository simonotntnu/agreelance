from django.test import TestCase, RequestFactory
from projects.views import get_user_task_permissions, task_permissions
from projects.models import Task, Project, ProjectCategory, TaskOffer
from user.models import Profile
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.contrib.messages.storage.fallback import FallbackStorage

class TaskPermissionsTestCase(TestCase):
    '''
    set up requestfactory
    set up category
    '''


    def setUp(self):
        self.factory = RequestFactory()

        category = ProjectCategory(name="testCategory")
        category.save()
        projectuser = User.objects.create_user(
            username='testProjectOwner',
            password='qwerty',
        )
        projectuser.save()
        projectOwner = Profile(
            id=1,
            user=projectuser,
        )
        projectOwner.categories.add(category)
        projectOwner.save()

        offereruser = User.objects.create_user(
            username='testProjectOfferer',
            password='qwerty',
        )
        offereruser.save()
        projectOfferer = Profile(
            id=2,
            user=offereruser,
        )
        projectOfferer.categories.add(category)
        projectOfferer.save()


        project = Project(
            id=1,
            user=projectOwner,
            title="testProject",
            description="test project description",
            category=category,
        )
        project.save()

        task = Task(
            id=1,
            project=project,
            title="test task",
            description="description for test task",
            budget=200
        )
        task.save()

        taskOffer = TaskOffer(
            task=task,
            title="Test Task Offer",
            description="description of offer",
            price=50,
            offerer=projectOfferer,
            status="a",
            feedback="test feedback"
        )
        taskOffer.save()

        randomuser = User.objects.create_user(
            username='testRandomUser',
            password='qwerty',
        )
        randomuser.save()
        randomProfile = Profile(
            id=3,
            user=randomuser,
        )
        randomProfile.categories.add(category)
        randomProfile.save()

        self.expected_permissions = {
            "write": False,
            "read": False,
            "modify": False,
            "owner": False,
            "upload": False,
        }
        self.random_user = Profile.objects.get(id=3).user

        self.task = Task.objects.get(title="test task")



        data = {
            # 3 here represents the id for random_user
            "user" : [3],
            "permission" : "Read",
        }
        self.request = self.get_request(data)

    def test_random_user_no_read(self):
        permissions = get_user_task_permissions(self.random_user, self.task)

        self.assertEqual(self.expected_permissions["read"], permissions["read"])

    def test_owner(self):
        '''
        tests that owner can give permission to other users
        '''
        self.expected_permissions["read"] = True

        owner_user = authenticate(username='testProjectOwner', password='qwerty')
        self.request.user = owner_user

        task_permissions(self.request, 1, 1)

        permissions = get_user_task_permissions(self.random_user, self.task)

        self.assertEqual(self.expected_permissions["read"], permissions["read"])

    def test_random_user(self):
        '''
        tests that user unrelated to task can not give himself permissions
        '''

        random_user = authenticate(username='testRandomUser', password='qwerty')
        self.request.user = random_user

        task_permissions(self.request, 1, 1)

        permissions = get_user_task_permissions(self.random_user, self.task)

        self.assertEqual(self.expected_permissions["read"], permissions["read"])


    def test_offerer(self):
        '''
        tests that accepted task offerer can give permission to other users
        '''

        self.expected_permissions["read"] = True

        offerer_user = authenticate(username='testProjectOfferer', password='qwerty')
        self.request.user = offerer_user

        task_permissions(self.request, 1, 1)

        permissions = get_user_task_permissions(self.random_user, self.task)

        self.assertEqual(self.expected_permissions["read"], permissions["read"])


    def get_request(self, data):
        request = self.factory.post("dummy_url", data)
        setattr(request, 'session', 'session')
        messages = FallbackStorage(request)
        setattr(request, '_messages', messages)
        return request
