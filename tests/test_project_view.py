from django.test import TestCase, RequestFactory
from projects.views import project_view
from projects.models import (
    Project,
    Task,
    TaskOffer,
    ProjectCategory
)
from django.contrib.auth.models import User
from user.models import Profile

class ProjectViewTestCase(TestCase):
    """
    Test coverage for submitting an offer, responding to an offer and
    changing the project status.

    """

    def setUp(self):
        """
        set up requestfactory
        set up category
        Set up project,  projectowner user,  offerer user.
        set up one tasks for the project.
        set up taskoffer.
        """

        self.factory = RequestFactory()
        category = ProjectCategory(name="testCategory")
        category.save()
        projectuser = User.objects.create_user(
            username='testProjectOwner',
            password='qwerty',
        )
        projectuser.save()
        projectOwner = Profile(
            id=1,
            user=projectuser,
        )
        projectOwner.categories.add(category)
        projectOwner.save()

        offereruser = User.objects.create_user(
            username='testProjectOfferer',
            password='qwerty',
        )
        offereruser.save()
        projectOfferer = Profile(
            id=2,
            user=offereruser,
        )
        projectOfferer.categories.add(category)
        projectOfferer.save()


        project = Project(
            user=projectOwner,
            title="testProject",
            description="test project description",
            category=category,
        )
        project.save()

        task = Task(
            project=project,
            title="test task",
            description="description for test task",
            budget=200
        )
        task.save()

        taskOffer = TaskOffer(
            task=task,
            title="Test Task Offer One",
            description="description of offer",
            price=50,
            offerer=projectOfferer,
            status="o",
            feedback="",
        )
        taskOffer.save()


    def  test_offer_response(self):
        """
        This test covers offer_response section in project view,
        up to and including acceping the offer. user is project owner.
        accepts the new offer.
        """
        user = Profile.objects.get(id=1).user
        data = {
            "offer_response" : "",
            "feedback" : "contains feedback",
            "taskofferid" : "1",
            "status" : "a"
        }
        request = self.factory.post("dummy_url", data)
        request.user = user
        response = project_view(request, 1)
        self.assertEqual(response.status_code, 200)
        offerstatus = TaskOffer.objects.get(id = 1).status
        self.assertEqual(offerstatus, "a")
        

    def test_status_change(self):
        """
        this test covers status_change section in project view,
         up to and including creating reviewable objects. (new feature)
         user is project owner. change status to finished.
        """
        user = Profile.objects.get(id=1).user 
        data = {
            "status": "f",
            "status_change" : ""
            
        }
        request = self.factory.post("dummy_url", data)
        request.user = user
        response = project_view(request, 1)
        status = Project.objects.get(pk= 1).status
        self.assertEqual(status, "f")
        self.assertEqual(response.status_code, 200)
        

    def test_offer_submit(self):
        """
        this test covers offer_submit section in project view,
        coverage for the offerer logic and returning different render.
        user is offerer.
        """
        user = Profile.objects.get(id=2).user
        data = {
            "title": " task offer title",
            "description" : "task offer description",
            "price" : "200",
            "taskvalue" : "1",
            "offer_submit" : ""
            
        }
        request = self.factory.post("dummy_url", data)
        request.user = user
        response = project_view(request, 1)
        new_offer = TaskOffer.objects.get(id = 2)
        self.assertTrue(new_offer)
        self.assertEqual(response.status_code, 200)