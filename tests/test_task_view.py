from django.test import TestCase, RequestFactory
from projects.views import task_view
from django.contrib.messages.storage.fallback import FallbackStorage
from projects.models import (
    Project,
    Task,
    TaskOffer,
    Delivery,
    ProjectCategory,
    Team,
    TaskFile,
    TaskFileTeam
)
from django.contrib.auth.models import User, AnonymousUser
from user.models import Profile
from django.core.files import temp as tempfile
from django.core.files.uploadedfile import SimpleUploadedFile

class TaskViewTestCase(TestCase):
    """
    Test coverage for submitting an offer, responding to an offer and
    changing the project status.

    """

    def setUp(self):
        """
        set up requestfactory
        set up category
        Set up project,  projectowner user,  offerer user.
        set up one tasks for the project.
        set up taskoffer.
        """

        self.factory = RequestFactory()
        category = ProjectCategory(name="testCategory")
        category.save()
        projectuser = User.objects.create_user(
            username='testProjectOwner',
            password='qwerty',
        )
        projectuser.save()
        projectOwner = Profile(
            id=1,
            user=projectuser,
       )
        
        projectOwner.categories.add(category)
        projectOwner.save()

        offereruser = User.objects.create_user(
            username='testProjectOfferer',
            password='qwerty',
        )
        offereruser.save()
        projectOfferer = Profile(
            id=2,
            user=offereruser,
        )
        projectOfferer.categories.add(category)
        projectOfferer.save()


        offereruser2 = User.objects.create_user(
            username='testProjectOfferer2',
            password='qwerty',
        )
        offereruser2.save()
        projectOfferer2 = Profile(
            id=3,
            user=offereruser2,
        )
        projectOfferer2.categories.add(category)
        projectOfferer2.save()
        project = Project(
            user=projectOwner,
            title="testProject",
            description="test project description",
            category=category,
        )
        project.save()

        task = Task(
            project=project,
            title="test task",
            description="description for test task",
            budget=200
        )
        task.save()

        taskOffer = TaskOffer(
            task=task,
            title="Test Task Offer One",
            description="description of offer",
            price=50,
            offerer=projectOfferer,
            status="a",
            feedback="",
        )
        task.read.add(projectOfferer)
        task.read.add(projectOfferer2)
        task.write.add(projectOfferer)
        project.participants.add(projectOfferer)
        taskOffer.save()


    def test_task_delivery(self):
        """
        attempts to deliver task
        """
        offerer = Profile.objects.get(id = 2)
        data = {
            "delivery" :"",
            "comment" : "test comment",
            "file" : ""
        }
        request = self.get_request(data)
        request.user = offerer.user

        tdir = tempfile.gettempdir()
        file = tempfile.NamedTemporaryFile(suffix=".file", dir=tdir)
        file.write(b'aaaaaaaaaaaaaaaaaaa')
        file.seek(0)

        with open(file.name, 'r') as f:
            request.FILES['file'] = SimpleUploadedFile(file.name, f.read().encode())


        resp = task_view(request, 1, 1)
        self.assertEqual(resp.status_code, 200)

        delivery = Delivery.objects.get(id = 1)
        self.assertEqual(delivery.status, "p")
        task = delivery.task
        self.assertEqual(task.status, "pa")
        

        
        

    def test_delivery_response(self):
        """
        test post delivery response
        """
        task = Task.objects.get(id = 1)
        offerer = Profile.objects.get(id = 2)
        projectowner = Profile.objects.get(id = 1)

        delivery = Delivery(
        task = task,
        delivery_user = offerer,
        id = 1
        )

        delivery.save()
        data = {
            "delivery-response" : "",
            "delivery-id" :   "1",
            "status" : 'a',
            "feedback" : "great, approved"
        }
        request =  self.get_request(data)
        request.user = projectowner.user
        resp = task_view(request, 1, 1)
        self.assertEqual(resp.status_code, 200)

        modified_delivery = Delivery.objects.get(id = 1)
        del_status = modified_delivery.status
        self.assertEqual(del_status, "a")
        task = Task.objects.get(id = 1)
        self.assertEqual(task.status, "pp")
        
        


    
    def test_team(self):
        """
        request:
        
        """

        data = {
            "team" : "",
            "name" : "testTeam"
        }
        request = self.get_request(data)
        request.user = Profile.objects.get(id = 2).user
        resp = task_view(request, 1, 1)
        self.assertEqual(resp.status_code, 200)
        count = Team.objects.all().count()
        self.assertEqual(count, 1)


    def test_team_add(self):

        offerer = Profile.objects.get(id = 2)
        offerer2 = Profile.objects.get(id = 3 )

        team = Team(
            name = "testTeam",
            task = Task.objects.get(id = 1)
        )

        team.save()
        

        members = Team.objects.get(id = 1).members.count()
        self.assertEqual(members, 0)
        data = {
            "team-add" : "",
            "team-id" : "1",
            "members" : ["2", "3"]
        }
        request = self.get_request(data)
        request.user = offerer.user
        resp = task_view(request, 1, 1)
        self.assertEqual(resp.status_code, 200)

        members = Team.objects.get(id = 1).members.count()
        self.assertEqual(members, 2)




    def test_tft_permissions(self):
        """
        tests changing team task file permessions
        """
        offerer = Profile.objects.get(id = 2)
        task = Task.objects.get(id = 1)
        data = {
            "permissions" : "",
            "permission-perobj-1-1" : "1",
            "permission-read-1-1": "True",
            "permission-write-1-1" : "True",
            "permission-modify-1-1" : "True",
            "permission-upload-1" : "True"
        }
        request = self.get_request(data)
        request.user = offerer.user

        tdir = tempfile.gettempdir()
        file = tempfile.NamedTemporaryFile(suffix=".file", dir=tdir)
        file.write(b'aaaaaaaaaaaaaaaaaaa')
        file.seek(0)

        with open(file.name, 'r') as f:
            taskFile = TaskFile(file = SimpleUploadedFile(file.name, f.read().encode()),
                            task = task)
            taskFile.save()


        team = Team(
            name = "testTeam",
            task = task
        )
        team.save()

        resp = task_view(request, 1, 1)
        self.assertEqual(resp.status_code, 200)
        tft = TaskFileTeam.objects.get(id = 1)
        self.assertTrue(tft.read)
        self.assertTrue(tft.write)
        self.assertTrue(tft.modify)
        team = Team.objects.get(id = 1)
        self.assertTrue(team.write)

    
    def test_not_authorized(self):
        """
        return redirect code
        """
        data = {
            "team-add" : "",
            "team-id" : "1",
            "members" : ["2", "3"]
        }
        request = self.get_request(data)
        request.user = AnonymousUser()
    
        resp = task_view(request, 1, 1)
        self.assertEqual(resp.status_code, 302)

    def get_request(self, data):
            request = self.factory.post("dummy_url", data)
            setattr(request, 'session', 'session')
            messages = FallbackStorage(request)
            setattr(request, '_messages', messages)
            return request


