from django.urls import path, include
from django.conf.urls import url
from . import views
from django.contrib.auth import views as auth_views
from user import views as user_views

urlpatterns = [
    path("", views.index, name="index"),
    path(
        "login/",
        auth_views.LoginView.as_view(template_name="user/login.html"),
        name="login",
    ),
    path("logout/", auth_views.LogoutView.as_view(next_page="/"), name="logout"),
    path("users/<str:user>/", user_views.profile, name="profile-view"),
    path("users/<str:user>/message", user_views.message, name="message-view"),
    path("users/<str:user>/inbox", user_views.inbox, name="inbox-view"),
    path(
        "users/<str:user>/deletemessageinbox/<int:messageid>",
        user_views.deletemessageinbox,
        name="deletemessageinbox-view",
    ),
    path("signup/", views.signup, name="signup"),
    url(r"^captcha/", include("captcha.urls")),
]
