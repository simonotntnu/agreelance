from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.validators import MaxValueValidator, MinValueValidator


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="profile")
    company = models.TextField(max_length=50, blank=True)
    phone_number = models.TextField(max_length=50, blank=True)
    country = models.TextField(max_length=50, blank=True)
    state = models.TextField(max_length=50, blank=True)
    city = models.TextField(max_length=50, blank=True)
    postal_code = models.TextField(max_length=50, blank=True)
    street_address = models.TextField(max_length=50, blank=True)
    categories = models.ManyToManyField(
        "projects.ProjectCategory", related_name="competance_categories"
    )

    def __str__(self):
        return self.user.username


# Used to see if a user is allowed to review another user
class Reviewable(models.Model):
    class Meta:
        unique_together = (('reviewer', 'reviewee'), ('reviewee', 'reviewer'))

    reviewer = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="reviewer"
    )
    reviewee = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="reviewee"
    )

    def create_reviewables(project, taskOffer):
        if not Reviewable.objects.filter(reviewer=project.user.user, reviewee=taskOffer.offerer.user):
            Reviewable.objects.create(reviewer=project.user.user, reviewee=taskOffer.offerer.user).save()
        if not Reviewable.objects.filter(reviewer=taskOffer.offerer.user, reviewee=project.user.user):
            Reviewable.objects.create(reviewer=taskOffer.offerer.user, reviewee=project.user.user).save()





# store review scores0
class Review(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="reviewed")
    reviewed_by = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="reviewed_by", null=True
    )
    score = models.IntegerField(
        default=1, validators=[MaxValueValidator(5), MinValueValidator(1)]
    )

    def __str__(self):
        return self.user.username


@receiver(post_save, sender=User)
def update_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
    instance.profile.save()  # Saves the user profile


class Message(models.Model):
    sender = models.ForeignKey(User, related_name="messagesender", on_delete=models.CASCADE)
    receiver = models.ForeignKey(
        User, related_name="messagereceiver", on_delete=models.CASCADE
    )
    message = models.TextField()
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"From {self.receiver} To {self.sender}"

