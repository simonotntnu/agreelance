from django.http import HttpResponse
from projects.models import ProjectCategory
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from django.shortcuts import render, redirect, get_object_or_404
from .models import User, Review, Message, Reviewable
from .forms import SignUpForm, ReviewForm, MessageForm

def calculate_score(reviews):
    total_score = 0
    score_nr = 0
    avg_score = 0
    for review in reviews:
        total_score += review.score
        score_nr += 1
    if score_nr != 0:
        avg_score = total_score / score_nr
    return avg_score

def index(request):
    return render(request, "base.html")


def signup(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            user.refresh_from_db()

            user.profile.company = form.cleaned_data.get("company")

            user.is_active = False
            user.profile.categories.add(*form.cleaned_data["categories"])
            user.save()
            raw_password = form.cleaned_data.get("password1")
            user = authenticate(username=user.username, password=raw_password)
            from django.contrib import messages

            messages.success(
                request, "Your account has been created and is awaiting verification."
            )
            return redirect("home")
    else:
        form = SignUpForm()
    return render(request, "user/signup.html", {"form": form})


def message(request, user):
    useracc = User.objects.filter(username=user).first()

    if request.method == "POST":
        form = MessageForm(request.POST)
        if form.is_valid() and useracc != request.user:
            message = form.save(commit=False)
            message.receiver = useracc
            message.sender = request.user
            message.save()
            return redirect("message-view", user=user)
    else:
        form = MessageForm()
    all_messages =  Message.objects.filter(receiver=useracc, sender=request.user) | \
                    Message.objects.filter(receiver=request.user, sender=useracc)
    all_messages = all_messages.order_by('created')
    context = {
        "all_messages" : all_messages,
        "form": form,
        "user": useracc,
    }
    return render(request, "user/message.html", context)


def inbox(request, user):
    useracc = User.objects.filter(username=user).first()
    if request.user.is_authenticated and request.user == useracc:
        convo_set = get_conversations(useracc)
        context = {
            "user": useracc,
            "convos" : convo_set,
        }
        return render(request, "user/inbox.html", context)

def get_conversations(user):
    all_messages =  Message.objects.filter(receiver=user) | \
                    Message.objects.filter(sender=user)
    convo_set = {'set', user}
    for directmessage in all_messages:
        convo_set.add(directmessage.sender)
        convo_set.add(directmessage.receiver)
    convo_set.remove(user)
    convo_set.remove('set')
    return convo_set


def deletemessageinbox(request, user, messageid):
    useracc = User.objects.filter(username=user).first()
    if request.user == useracc:
        message = Message.objects.filter(id=messageid)
        message.delete()
        messages.success(request, f"You deleted a message!")
        return redirect("inbox-view", user=user)


def save_review(reviewform, reviewed, reviewer):
        review = reviewform.save(commit=False)
        review.user = reviewed
        review.reviewed_by = reviewer
        review.save()

def profile(request, user):
    useracc = User.objects.filter(username=user).first()

    if not useracc:
        messages.error(request, "No such user exists!")
        return redirect("/")

    can_review = False
    if request.user.is_authenticated:
        prev_instance = Review.objects.filter(
            user = useracc,
            reviewed_by=request.user,
        ).first()

        reviewable = Reviewable.objects.filter(
            reviewer=request.user,
            reviewee=useracc,
        ).first()

        if reviewable:
            can_review = True

        if request.user != useracc and request.method == "POST" and can_review:
            reviewform = ReviewForm(request.POST, instance=prev_instance)
            if reviewform.is_valid():
                save_review(reviewform, useracc, request.user)
                reviewable.delete()
                messages.success(request, f"You have reviewed {useracc.username}!")
                return redirect("profile-view", user=user)
        else:
            reviewform = ReviewForm(instance=prev_instance)
    else:
        reviewform = ReviewForm()

    avg_score = calculate_score(Review.objects.filter(user=useracc))

    context = {
        "user": useracc,
        "reviewform": reviewform,
        "score": round(avg_score, 1),
        "can_review": can_review,
    }


    return render(request, "user/profile.html", context)

